package com.atlassian.stash.rest.client.api.entity;

/**
 * The anchor of a task, such as a pull request's comment.
 */
public interface TaskAnchor {
    /**
     * @return the anchor's ID
     */
    long getId();

    enum Type {
        COMMENT
    }
}
