package com.atlassian.stash.rest.client.api;

import javax.annotation.Nullable;

/**
 * can be used on few methods in {@link StashClient} to request remote Stash/Bitbucket instance to return
 * avatar data.  Can specify size, URL scheme and URL mode of the avatar links returned
 */
public interface AvatarRequest {
    /**
     * @return the height/width of the avatar being requested
     */
    long getSize();

    /**
     * Note size must be specified for scheme to be honoured, at least as of Bitbucket 4.10
     *
     * @return <code>https</code> for secure https URLs, rather than request specific (as of Bitbucket 4.12.0).
     */
    @Nullable
    String getUrlScheme();

    /**
     * Note size must be specified for scheme to be honoured, at least as of Bitbucket 4.10
     *
     * @return <code>absolute</code> for absolute URLs, rather than request specific (as of Bitbucket 4.12.0).
     */
    @Nullable
    String getUrlMode();

    static DefaultAvatarRequest.DefaultAvatarRequestBuilder builder() {
        return new DefaultAvatarRequest.DefaultAvatarRequestBuilder();
    }

    class DefaultAvatarRequest implements AvatarRequest {
        private final long size;
        @Nullable
        private final String urlScheme;
        @Nullable
        private final String urlMode;

        DefaultAvatarRequest(final long size, @Nullable final String urlScheme, @Nullable final String urlMode) {
            this.size = size;
            this.urlScheme = urlScheme;
            this.urlMode = urlMode;
        }

        @Override
        public long getSize() {
            return size;
        }

        @Override
        @Nullable
        public String getUrlScheme() {
            return urlScheme;
        }

        @Override
        @Nullable
        public String getUrlMode() {
            return urlMode;
        }

        public static class DefaultAvatarRequestBuilder {
            private long size = Long.MIN_VALUE;
            private boolean secure = false;
            private boolean absolute = false;

            public DefaultAvatarRequestBuilder size(final long size) {
                this.size = size;
                return this;
            }

            public DefaultAvatarRequestBuilder secure(final boolean secure) {
                this.secure = secure;
                return this;
            }

            public DefaultAvatarRequestBuilder absolute(final boolean absolute) {
                this.absolute = absolute;
                return this;
            }

            public AvatarRequest.DefaultAvatarRequest build() {
                return new AvatarRequest.DefaultAvatarRequest(size,
                        secure ? "https" : null,
                        absolute ? "absolute" : null);
            }
        }
    }
}