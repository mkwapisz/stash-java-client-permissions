package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

/**
 *
 * @author Marcin Kwapisz
 */
public class Group {

    private final String name;

    public Group(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("name", name)
                .toString();
    }

}
