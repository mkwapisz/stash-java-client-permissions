package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Group permission to a repository. Contains name of a group and its {@link Permission}
 * @author Marcin Kwapisz
 */
public class RepositoryGroupPermission {

    @Nonnull
    private final String groupName;
    
    @Nullable
    private final Permission permission;

    public RepositoryGroupPermission(@Nonnull String groupName, @Nullable Permission permission) {
        this.groupName = groupName;
        this.permission = permission;
    }

    public String getGroupName() {
        return groupName;
    }

    public Permission getPermission() {
        return permission;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("group", groupName)
                .add("permission", permission)
                .toString();
    }
}
