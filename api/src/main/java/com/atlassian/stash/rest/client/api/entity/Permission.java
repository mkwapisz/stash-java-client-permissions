package com.atlassian.stash.rest.client.api.entity;

/**
 * Repository permissions available in Stash
 */
public enum Permission {
    /**
     * Allows read access to a repository.
     * <p>
     * This allows cloning and pulling changes from a repository, adding comments and declining pull requests
     * that target the repository. It also allows creating pull requests if the user has the permission
     * on <em>both</em> the source and target repository.
     *
     * See com.atlassian.stash.user.Permission.REPO_READ
     */
    REPO_READ,
    /**
     * Allows write access to a repository.
     * <p>
     * In addition to the permissions already granted by {@link #REPO_READ}, this allows pushing changes
     * to a repository and merging pull requests targeting the repository.
     *
     * See com.atlassian.stash.user.Permission.REPO_WRITE
     */
    REPO_WRITE,
    /**
     * Allows to administer a repository.
     * <p>
     * In addition to the permissions already granted by {@link #REPO_WRITE}, this allows accessing and updating
     * the configuration of the repository, such as adding or revoking branch permissions, adding or revoking other
     * repository permissions, renaming or deleting the repository.
     *
     * See com.atlassian.stash.user.Permission.REPO_ADMIN
     */
    REPO_ADMIN,
}
