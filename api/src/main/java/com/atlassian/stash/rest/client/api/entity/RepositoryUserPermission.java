package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * User permission to a repository. Contains {@link User} object and assigned {@link Permission}
 * @author Marcin Kwapisz
 */
public class RepositoryUserPermission {

    @Nonnull
    private final User user;
    
    @Nullable
    private final Permission permission;

    public RepositoryUserPermission(@Nonnull User user, @Nullable Permission permission) {
        this.user = user;
        this.permission = permission;
    }

    public User getUser() {
        return user;
    }

    public String getUserName() {
        return (user != null) ? user.getName() : null;
    }

    public Permission getPermission() {
        return permission;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("user", user)
                .add("permission", permission)
                .toString();
    }
}
