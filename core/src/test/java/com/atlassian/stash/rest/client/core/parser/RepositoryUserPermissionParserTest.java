package com.atlassian.stash.rest.client.core.parser;

import static com.atlassian.stash.rest.client.api.EntityMatchers.repositoryUserPermission;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.RepositoryUserPermission;
import com.atlassian.stash.rest.client.core.TestData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Marcin Kwapisz
 */
public class RepositoryUserPermissionParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private RepositoryUserPermissionParser rupp;

    private JsonArray values;

    @Before
    public void before() {

        rupp = new RepositoryUserPermissionParser();
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(TestData.REPOSITORY_USER_PERMISSIONS_PARSER);
        values = jo.getAsJsonArray("values");

    }

    @Test
    public void testApply() {
        RepositoryUserPermission repositoryUserPermission = rupp.apply(values.get(0));
        assertThat(repositoryUserPermission, repositoryUserPermission()
            .user(is("repoReadUser"))
            .permission(is(Permission.REPO_READ))
            .build()
        );
        
        repositoryUserPermission = rupp.apply(values.get(1));
        assertThat(repositoryUserPermission, repositoryUserPermission()
            .user(is("repoWriteUser"))
            .permission(is(Permission.REPO_WRITE))
            .build()
        );
        
        repositoryUserPermission = rupp.apply(values.get(2));
        assertThat(repositoryUserPermission, repositoryUserPermission()
            .user(is("repoAdminUser"))
            .permission(is(Permission.REPO_ADMIN))
            .build()
        );
        
        repositoryUserPermission = rupp.apply(values.get(3));
        assertThat(repositoryUserPermission, repositoryUserPermission()
            .user(is("repoFailUser"))
            .permission(nullValue())
            .build()
        );
    }

}
