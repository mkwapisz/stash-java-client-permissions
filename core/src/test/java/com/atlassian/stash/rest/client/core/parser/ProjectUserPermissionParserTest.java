package com.atlassian.stash.rest.client.core.parser;

import static com.atlassian.stash.rest.client.api.EntityMatchers.projectUserPermission;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectUserPermission;
import com.atlassian.stash.rest.client.core.TestData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ProjectUserPermissionParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private ProjectUserPermissionParser pupp;

    private JsonArray values;

    @Before
    public void before() {

        pupp = new ProjectUserPermissionParser();
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject)jsonParser.parse(TestData.PROJECT_USER_PERMISSIONS_PARSER);
        values = jo.getAsJsonArray("values");

    }

    @Test
    public void testApply() throws Exception {

        ProjectUserPermission projectUserPermission = pupp.apply(values.get(0));
        assertThat(projectUserPermission, projectUserPermission()
            .userName(is("footer"))
            .permission(is(ProjectPermission.PROJECT_ADMIN))
            .build()
        );

        projectUserPermission = pupp.apply(values.get(1));
        assertThat(projectUserPermission, projectUserPermission()
            .userName(is("foo"))
            .permission(is(ProjectPermission.PROJECT_READ))
            .build()
        );

        projectUserPermission = pupp.apply(values.get(2));
        assertThat(projectUserPermission, projectUserPermission()
            .userName(is("writer"))
            .permission(is(ProjectPermission.PROJECT_WRITE))
            .build()
        );

        projectUserPermission = pupp.apply(values.get(5));
        assertThat(projectUserPermission, projectUserPermission()
            .userName(is("wrongPerm"))
            .permission(nullValue())
            .build()
        );

    }


    @Test
    public void testApply_NoUserElement() throws Exception {

        thrown.expect(NullPointerException.class);
        pupp.apply(values.get(4));

    }

    @Test
    public void testApply_NoUserNameElement() throws Exception {

        thrown.expect(NullPointerException.class);
        pupp.apply(values.get(3));
    }

}
