package com.atlassian.stash.rest.client.core.parser;

import static com.atlassian.stash.rest.client.api.EntityMatchers.projectGroupPermission;
import static com.atlassian.stash.rest.client.api.EntityMatchers.repositoryGroupPermission;
import com.atlassian.stash.rest.client.api.entity.Permission;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.atlassian.stash.rest.client.api.entity.RepositoryGroupPermission;
import com.atlassian.stash.rest.client.core.TestData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class RepositoryGroupPermissionParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private RepositoryGroupPermissionParser repositoryGroupPermissionParser;

    private JsonArray values;

    @Before
    public void before() {

        repositoryGroupPermissionParser = new RepositoryGroupPermissionParser();
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject)jsonParser.parse(TestData.REPOSITORY_GROUP_PERMISSIONS_PARSER);
        values = jo.getAsJsonArray("values");

    }

    @Test
    public void testApply() throws Exception {

        RepositoryGroupPermission repositoryGroupPermission = repositoryGroupPermissionParser.apply(values.get(0));
        assertThat(repositoryGroupPermission, repositoryGroupPermission()
            .groupName(is("groupRead"))
            .permission(is(Permission.REPO_READ))
            .build()
        );

        repositoryGroupPermission = repositoryGroupPermissionParser.apply(values.get(1));
        assertThat(repositoryGroupPermission, repositoryGroupPermission()
            .groupName(is("groupWrite"))
            .permission(is(Permission.REPO_WRITE))
            .build()
        );

        repositoryGroupPermission = repositoryGroupPermissionParser.apply(values.get(2));
        assertThat(repositoryGroupPermission, repositoryGroupPermission()
            .groupName(is("groupAdmin"))
            .permission(is(Permission.REPO_ADMIN))
            .build()
        );

        repositoryGroupPermission = repositoryGroupPermissionParser.apply(values.get(3));
        assertThat(repositoryGroupPermission, repositoryGroupPermission()
            .groupName(is("groupFail"))
            .permission(nullValue())
            .build()
        );

    }


    @Test
    public void testApply_NoGroupNameElement() throws Exception {

        thrown.expect(NullPointerException.class);
        repositoryGroupPermissionParser.apply(values.get(4));

    }

    @Test
    public void testApply_NoGroupElement() throws Exception {

        thrown.expect(NullPointerException.class);
        repositoryGroupPermissionParser.apply(values.get(5));
    }


}
