package com.atlassian.stash.rest.client.core.parser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ProjectGroupPermissionParser extends ProjectPermissionParser<ProjectGroupPermission> {

    @Nonnull
    @Override
    public ProjectGroupPermission apply(@Nullable final JsonElement pgPermission)  {

        final JsonObject jsonObject = getPgPermission(pgPermission);

        final String groupName = getNameAttributeFromObject(jsonObject, "group");
        final String permissionRaw = jsonObject.get("permission").getAsString();

        return new ProjectGroupPermission(groupName, mapProjectPermission(permissionRaw));
    }
}
