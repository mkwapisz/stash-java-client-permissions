package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.RepositoryUserPermission;
import com.atlassian.stash.rest.client.api.entity.User;
import static com.atlassian.stash.rest.client.core.parser.Parsers.userParser;
import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

/**
 *
 * @author Marcin Kwapisz
 */
public class RepositoryUserPermissionParser implements Function<JsonElement, RepositoryUserPermission> {
    
    private static final Logger LOG = Logger.getLogger(RepositoryUserPermissionParser.class);
    
    @Override
    public RepositoryUserPermission apply(JsonElement json) {
        if (json == null || !json.isJsonObject()) {
            return null;
        }

        JsonObject jsonPermission = json.getAsJsonObject();
        
        User user = userParser().apply(jsonPermission.get("user"));
        String permissionName = jsonPermission.get("permission").getAsString();
        Permission permission = null;
        try {
            permission = Permission.valueOf(permissionName);
        } catch (IllegalArgumentException iae) {
            LOG.warn(iae);
        }
        
        return new RepositoryUserPermission(user, permission);
    }
}
