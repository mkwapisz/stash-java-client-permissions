package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.RepositoryGroupPermission;
import static com.atlassian.stash.rest.client.core.parser.Parsers.groupParser;
import com.google.common.base.Preconditions;
import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

/**
 *
 * @author Marcin Kwapisz
 */
public class RepositoryGroupPermissionParser implements Function<JsonElement, RepositoryGroupPermission> {

    private static final Logger LOG = Logger.getLogger(RepositoryGroupPermissionParser.class);

    @Override
    public RepositoryGroupPermission apply(final JsonElement json) {

        JsonObject jsonPermission = Preconditions.checkNotNull(json.getAsJsonObject(), "RepositoryGroupPermission");

        String groupName = groupParser().apply(jsonPermission.get("group"));
        String permissionName = jsonPermission.get("permission").getAsString();
        Permission permission = null;
        try {
            permission = Permission.valueOf(permissionName);
        } catch (IllegalArgumentException iae) {
            LOG.warn(iae);
        }

        return new RepositoryGroupPermission(groupName, permission);
    }
}
