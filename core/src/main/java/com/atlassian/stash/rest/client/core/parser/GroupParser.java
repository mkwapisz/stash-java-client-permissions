package com.atlassian.stash.rest.client.core.parser;

import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.function.Function;

/**
 *
 * @author Marcin Kwapisz
 */
class GroupParser implements Function<JsonElement, String> {

    public GroupParser() {
    }

    @Override
    public String apply(JsonElement json) {
        JsonObject jsonObject = Preconditions.checkNotNull(json.getAsJsonObject(), "Group");
        String retValue = Preconditions.checkNotNull(jsonObject.getAsJsonPrimitive("name"), "groupName").getAsString();
        return retValue;
    }

}
