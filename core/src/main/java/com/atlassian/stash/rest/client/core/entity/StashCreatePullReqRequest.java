package com.atlassian.stash.rest.client.core.entity;

import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.stream.StreamSupport;

public class StashCreatePullReqRequest {
    @Nonnull
    private final String title;
    @Nullable
    private final String description;
    @Nonnull
    private final PullRequestRef fromRef;
    @Nonnull
    private final PullRequestRef toRef;
    @Nonnull
    private final ImmutableList<String> reviewers;

    public StashCreatePullReqRequest(@Nonnull final String title, @Nullable final String description,
                                     @Nonnull final PullRequestRef fromRef, @Nonnull final PullRequestRef toRef,
                                     @Nonnull final Iterable<String> reviewers) {
        this.title = title;
        this.description = description;
        this.fromRef = fromRef;
        this.toRef = toRef;
        this.reviewers = ImmutableList.copyOf(reviewers);
    }

    public JsonObject toJson() {
        final JsonObject req = new JsonObject();

        req.addProperty("title", title);
        req.addProperty("description", description);
        req.add("fromRef", refToJson(fromRef));
        req.add("toRef", refToJson(toRef));
        req.add("reviewers", reviewersToJson(reviewers));

        return req;
    }

    private JsonElement refToJson(@Nonnull final PullRequestRef ref) {
        final JsonObject json = new JsonObject();

        json.addProperty("id", ref.getId());
        json.add("repository", refToRepositoryJson(ref));

        return json;
    }

    private JsonElement refToRepositoryJson(@Nonnull final PullRequestRef ref) {
        final JsonObject json = new JsonObject();

        json.addProperty("slug", ref.getRepositorySlug());
        json.add("project", refToProjectJson(ref));

        return json;
    }

    private JsonElement refToProjectJson(@Nonnull final PullRequestRef ref) {
        final JsonObject json = new JsonObject();

        json.addProperty("key", ref.getProjectKey());

        return json;
    }

    private JsonElement reviewersToJson(@Nonnull final Iterable<String> reviewers) {
        final JsonArray json = new JsonArray();

        StreamSupport.stream(reviewers.spliterator(), false)
                .map(this::reviewerToJson)
                .forEach(json::add);

        return json;
    }

    private JsonElement reviewerToJson(@Nonnull final String reviewer) {
        final JsonObject userNameJson = new JsonObject();
        userNameJson.addProperty("name", reviewer);

        final JsonObject userJson = new JsonObject();
        userJson.add("user", userNameJson);
        return userJson;
    }
}
