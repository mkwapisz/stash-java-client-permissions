package com.atlassian.stash.rest.client.core.entity;


import com.google.gson.JsonObject;

public class StashUpdateProjectRequest {

    private final String key;
    private final String name;
    private final String description;

    public StashUpdateProjectRequest(final String key, final String name, final String description) {
        this.key = key;
        this.name = name;
        this.description = description;
    }

    public JsonObject toJson() {
        JsonObject req = new JsonObject();

        req.addProperty("key", key);
        req.addProperty("name", name);
        req.addProperty("description", description);

        return req;
    }
}