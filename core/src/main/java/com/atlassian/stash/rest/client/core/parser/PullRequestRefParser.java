package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.function.Function;

public class PullRequestRefParser implements Function<JsonElement, PullRequestRef> {
    @Override
    public PullRequestRef apply(@Nullable JsonElement pullRequestRef) {
        final JsonObject jsonObject = Preconditions.checkNotNull(pullRequestRef, "pullRequestRef").getAsJsonObject();

        final JsonObject repository = Preconditions.checkNotNull(jsonObject.getAsJsonObject("repository"), "repository");
        final JsonObject project = Preconditions.checkNotNull(repository.getAsJsonObject("project"), "project");

        final String id = jsonObject.get("id").getAsString();
        final String displayId = jsonObject.get("displayId").getAsString();
        final String repositorySlug = repository.get("slug").getAsString();
        final String repositoryName = repository.get("name").getAsString();
        final String projectKey = project.get("key").getAsString();
        final String projectName = project.get("name").getAsString();

        return new PullRequestRef(repositorySlug, repositoryName, projectKey, projectName, id, displayId);
    }
}
