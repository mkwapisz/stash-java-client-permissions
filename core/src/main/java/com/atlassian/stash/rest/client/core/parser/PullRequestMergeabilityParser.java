package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.function.Function;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonString;

public class PullRequestMergeabilityParser implements Function<JsonElement, PullRequestMergeability> {
    @Override
    public PullRequestMergeability apply(@Nullable final JsonElement prMergeability) {
        final JsonObject jsonObject = Preconditions.checkNotNull(prMergeability, "prMergeability").getAsJsonObject();

        final Optional<String> outcome = Optional.ofNullable(getOptionalJsonString(jsonObject, "outcome"));
        final boolean canMerge = jsonObject.getAsJsonPrimitive("canMerge").getAsBoolean();
        final boolean conflicted = jsonObject.getAsJsonPrimitive("conflicted").getAsBoolean();

        return new PullRequestMergeability(outcome, canMerge, conflicted);
    }
}