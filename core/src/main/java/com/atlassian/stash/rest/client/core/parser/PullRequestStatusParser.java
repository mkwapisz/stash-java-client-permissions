package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.PullRequestParticipant;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.linkToHref;
import static com.atlassian.stash.rest.client.core.parser.Parsers.linkParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.listParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.pullRequestParticipantParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.pullRequestRefParser;

public class PullRequestStatusParser implements Function<JsonElement, PullRequestStatus> {
    @Override
    public PullRequestStatus apply(@Nullable final JsonElement pullRequest) {
        final JsonObject jsonObject = Preconditions.checkNotNull(pullRequest, "pullRequest").getAsJsonObject();

        final long id = jsonObject.get("id").getAsLong();
        final long version = jsonObject.get("version").getAsLong();
        final String title = jsonObject.get("title").getAsString();
        final Optional<String> description = jsonObject.has("description")
                ? Optional.of(jsonObject.get("description").getAsString())
                : Optional.empty();

        final String url = selfUrlParser.apply(pullRequest);
        final String state = jsonObject.get("state").getAsString();
        final long lastUpdated = jsonObject.get("updatedDate").getAsLong();

        final PullRequestParticipant author = authorParser.apply(pullRequest);
        Preconditions.checkState("AUTHOR".equals(author.getRole()), "author has valid role");

        final List<PullRequestParticipant> reviewers = reviewersParser.apply(pullRequest);
        final PullRequestRef fromRef = pullRequestRefParser().apply(jsonObject.get("fromRef").getAsJsonObject());
        final PullRequestRef toRef = pullRequestRefParser().apply(jsonObject.get("toRef").getAsJsonObject());

        final Optional<JsonObject> attributes = Optional.ofNullable(jsonObject.getAsJsonObject("attributes"));
        // as of BBS 4.0 task and comment counts are returned in "properties" rather than "attributes"
        final Optional<JsonObject> properties = Optional.ofNullable(jsonObject.getAsJsonObject("properties"));

        final Optional<Long> commentCountFromAttributes = getPropertyFirstElementArrayAsLong(attributes, "commentCount");
        final Optional<Long> resolvedTaskCountFromAttributes = getPropertyFirstElementArrayAsLong(attributes, "resolvedTaskCount");
        final Optional<Long> openTaskCountFromAttributes = getPropertyFirstElementArrayAsLong(attributes, "openTaskCount");

        final Optional<Long> commentCountFromProperties = getPropertyAsLong(properties, "commentCount");
        final Optional<Long> resolvedTaskCountFromProperties = getPropertyAsLong(properties, "resolvedTaskCount");
        final Optional<Long> openTaskCountFromProperties = getPropertyAsLong(properties, "openTaskCount");

        final Optional<Long> commentCount = firstNonEmptyOptionalOrEmpty(commentCountFromAttributes, commentCountFromProperties);
        final Optional<Long> openTaskCount = firstNonEmptyOptionalOrEmpty(openTaskCountFromAttributes, openTaskCountFromProperties);
        final Optional<Long> resolvedTaskCount = firstNonEmptyOptionalOrEmpty(resolvedTaskCountFromAttributes, resolvedTaskCountFromProperties);

        final Optional<String> mergeOutcome = properties.flatMap(mergeOutcomeParser);

        return new PullRequestStatus(id, version, title, description, url, state, mergeOutcome, author, reviewers,
                fromRef, toRef, lastUpdated, commentCount, openTaskCount, resolvedTaskCount);
    }

    @SafeVarargs
    private final <T> Optional<T> firstNonEmptyOptionalOrEmpty(final Optional<T>... optionals) {
        for (Optional<T> optional : optionals) {
            if (optional.isPresent()) {
                return optional;
            }
        }
        return Optional.empty();
    }

    private Optional<Long> getPropertyAsLong(final Optional<JsonObject> properties, final String attributeName) {
        return properties
                .map(propsJson -> propsJson.get(attributeName))
                .map(JsonElement::getAsLong);
    }

    private Optional<Long> getPropertyFirstElementArrayAsLong(final Optional<JsonObject> attributes, final String propertyName) {
        return attributes
                .map(attrsJson -> attrsJson.getAsJsonArray(propertyName))
                .filter(attrsJsonArray -> attrsJsonArray.size() > 0)
                .map(attrsJsonArray -> attrsJsonArray.get(0))
                .map(JsonElement::getAsString)
                .map(Long::parseLong);
    }

    private static Function<JsonElement, List<PullRequestParticipant>> reviewersParser = pullRequest -> {
        final JsonObject jsonObject = Preconditions.checkNotNull(pullRequest, "pullRequest").getAsJsonObject();
        final JsonElement reviewersJsonArray = Preconditions.checkNotNull(jsonObject.get("reviewers"), "reviewers");
        return listParser(pullRequestParticipantParser()).apply(reviewersJsonArray);
    };

    private static Function<JsonElement, PullRequestParticipant> authorParser = pullRequest -> {
        final JsonObject jsonObject = Preconditions.checkNotNull(pullRequest, "pullRequest").getAsJsonObject();
        final JsonObject authorJson = Preconditions.checkNotNull(jsonObject.getAsJsonObject("author"), "author");
        return pullRequestParticipantParser().apply(authorJson);
    };

    private static Function<JsonElement, String> selfUrlParser = pullRequest -> {
        final JsonObject jsonObject = Preconditions.checkNotNull(pullRequest, "pullRequest").getAsJsonObject();
        final JsonObject links = Preconditions.checkNotNull(jsonObject.getAsJsonObject("links"), "links");
        List<Link> selfLinks = listParser(linkParser("href", "name")).apply(links.get("self"));
        return selfLinks.stream().map(l -> linkToHref().apply(l)).findFirst().orElse(null);
    };

    private static Function<? super JsonObject, Optional<String>> mergeOutcomeParser = properties -> Optional.ofNullable(properties)
            .map(propsJson -> propsJson.getAsJsonObject("mergeResult"))
            .map(propsJson -> propsJson.get("outcome"))
            .map(JsonElement::getAsString);
}
