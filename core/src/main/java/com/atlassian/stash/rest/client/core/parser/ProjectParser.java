package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Project;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Function;

class ProjectParser implements Function<JsonElement, Project> {
    @Override
    public Project apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        String selfUrl = null;

        if (jsonObject.has("links")) {
            JsonObject links = jsonObject.getAsJsonObject("links");
            selfUrl = ParserUtil.getNamedLink(links, "self");
        }

        return new Project(
                jsonObject.get("key").getAsString(),
                jsonObject.get("id").getAsLong(),
                jsonObject.get("name").getAsString(),
                jsonObject.has("description") ? jsonObject.get("description").getAsString() : null,
                jsonObject.has("public") && jsonObject.get("public").getAsBoolean(),
                jsonObject.has("isPersonal") && jsonObject.get("isPersonal").getAsBoolean(),
                jsonObject.get("type").getAsString(),
                selfUrl
                );
    }

}
