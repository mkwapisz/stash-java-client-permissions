package com.atlassian.stash.rest.client.core.entity;

import com.atlassian.stash.rest.client.api.StashException;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.TaskAnchor;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;

public class CreateTaskRequest {
    private final long anchorId;
    @Nonnull
    private final TaskAnchor.Type anchorType;
    @Nonnull
    public final String text;

    private CreateTaskRequest(final long anchorId, @Nonnull final TaskAnchor.Type anchorType, @Nonnull final String text) {
        this.anchorId = anchorId;
        this.anchorType = anchorType;
        this.text = text;
    }

    public static CreateTaskRequest forComment(@Nonnull final Comment comment, @Nonnull final String text) {
        return new CreateTaskRequest(comment.getId(), TaskAnchor.Type.COMMENT, text);
    }

    public static CreateTaskRequest forAnchor(@Nonnull final TaskAnchor anchor, @Nonnull final String text) {
        if (anchor instanceof Comment) {
            return forComment((Comment) anchor, text);
        } else {
            throw new StashException("Unknown task anchor of class: " + anchor.getClass());
        }
    }

    public JsonObject toJson() {
        final JsonObject req = new JsonObject();

        req.addProperty("text", text);
        req.add("anchor", anchorToJson());

        return req;
    }

    private JsonElement anchorToJson() {
        final JsonObject req = new JsonObject();

        req.addProperty("id", anchorId);
        req.addProperty("type", anchorType.name());

        return req;
    }
}