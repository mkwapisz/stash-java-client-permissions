package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.function.Function;

public class ApplicationPropertiesParser implements Function<JsonElement, ApplicationProperties> {
    @Override
    public ApplicationProperties apply(@Nullable final JsonElement jsonElement) {
        final JsonObject jsonObject = Preconditions.checkNotNull(jsonElement, "applicationProperties").getAsJsonObject();

        final String version = Preconditions.checkNotNull(jsonObject.get("version"), "version").getAsString();
        final String buildNumber = Preconditions.checkNotNull(jsonObject.get("buildNumber"), "buildNumber").getAsString();
        final String buildDate = Preconditions.checkNotNull(jsonObject.get("buildDate"), "buildDate").getAsString();
        final String displayName = Preconditions.checkNotNull(jsonObject.get("displayName"), "displayName").getAsString();

        return new ApplicationProperties(version, buildNumber, buildDate, displayName);
    }
}