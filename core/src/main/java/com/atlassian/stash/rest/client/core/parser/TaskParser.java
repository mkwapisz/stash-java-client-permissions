package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Task;
import com.atlassian.stash.rest.client.api.entity.TaskAnchor;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Function;

public class TaskParser implements Function<JsonElement, Task> {

    private static final TaskAnchor UNKNOWN_ANCHOR = () -> Long.MIN_VALUE;

    @Override
    public Task apply(@Nullable final JsonElement task) {
        final JsonObject jsonObject = Preconditions.checkNotNull(task, "task").getAsJsonObject();

        final long id = Preconditions.checkNotNull(jsonObject.get("id"), "id").getAsLong();
        final String text = Preconditions.checkNotNull(jsonObject.get("text"), "text").getAsString();
        final String state = Preconditions.checkNotNull(jsonObject.get("state"), "state").getAsString();
        final JsonObject anchorJson = Preconditions.checkNotNull(jsonObject.get("anchor"), "anchor").getAsJsonObject();
        final TaskAnchor anchor = anchorToJson(anchorJson);

        return new Task(id, text,  state, anchor);

    }

    private TaskAnchor anchorToJson(@Nonnull final JsonObject anchorJson) {
        final String type = Preconditions.checkNotNull(anchorJson.get("type"), "anchor type").getAsString();

        switch (type) {
            case "COMMENT":
                return Parsers.commentParser().apply(anchorJson);
            default:
                return UNKNOWN_ANCHOR;
        }
    }
}
