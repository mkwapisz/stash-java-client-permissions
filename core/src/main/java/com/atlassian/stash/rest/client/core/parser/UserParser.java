package com.atlassian.stash.rest.client.core.parser;

import java.util.Date;
import java.util.function.Function;

import javax.annotation.Nonnull;

import com.atlassian.stash.rest.client.api.entity.User;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.Optional;

public class UserParser implements Function<JsonElement, User> {

    @Override
    @Nonnull
    public User apply(JsonElement pgUser)  {
        final JsonObject jsonObject = Preconditions.checkNotNull(pgUser, "pgUser").getAsJsonObject();

        final String name =  Preconditions.checkNotNull(jsonObject.get("name"), "name").getAsString();
        final long id = Preconditions.checkNotNull(jsonObject.get("id"), "id").getAsLong();
        final String displayName =  Preconditions.checkNotNull(jsonObject.get("displayName"), "displayName").getAsString();
        final String slug =  Preconditions.checkNotNull(jsonObject.get("slug"), "slug").getAsString();
        final boolean active = Preconditions.checkNotNull(jsonObject.get("active"), "active").getAsBoolean();
        final boolean mutableDetails = Optional.ofNullable(jsonObject.get("mutableDetails")).orElse(new JsonPrimitive(false)).getAsBoolean();
        final boolean mutableGroups = Optional.ofNullable(jsonObject.get("mutableGroups")).orElse(new JsonPrimitive(false)).getAsBoolean();
        final String userType = Preconditions.checkNotNull(jsonObject.get("type"), "type").getAsString();

        final String emailAddress = ParserUtil.getOptionalJsonString(jsonObject, "emailAddress");
        final String directoryName = ParserUtil.getOptionalJsonString(jsonObject, "directoryName");
        final boolean deletable = getDeletableProperty(jsonObject);
        final Date lastAuthenticationTimestamp = getLastAuthenticationTimestamp(jsonObject);
        final String selfUrl = getSelfUrl(jsonObject);

        return new User(name, emailAddress, id, displayName, active, slug, userType, directoryName, deletable,
                        lastAuthenticationTimestamp, mutableDetails, mutableGroups, selfUrl);
    }

    private String getSelfUrl(final JsonObject jsonObject) {
        final JsonObject jsonElement = jsonObject.getAsJsonObject("links");
        if (jsonElement == null) {
            return null;
        }
        return ParserUtil.getNamedLink(jsonElement, "self");
    }

    private boolean getDeletableProperty(final JsonObject jsonObject) {
        // for older versions of stash before 3.10.0 assume that the users are always not deletable
        Boolean deletable = ParserUtil.getOptionalJsonBoolean(jsonObject, "deletable");
        if (deletable == null) {
          return false;
        }
        return deletable;
    }

    private Date getLastAuthenticationTimestamp(JsonObject jsonObject) {
        final Long lastAuthenticationTimestamp = ParserUtil.getOptionalJsonLong(jsonObject, "lastAuthenticationTimestamp");
        if (lastAuthenticationTimestamp == null) {
            return null;
        }
        return new Date(lastAuthenticationTimestamp);
    }
}
